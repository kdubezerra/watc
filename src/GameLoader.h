//
// C++ Interface: GameLoader
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GAMELOADER_H
#define GAMELOADER_H
#include "InGE.h"
#include <GL/glut.h>

#include "AvatarWATC.h"
#include "GlutLabel.h"
#include "GlutMenuItem.h"
#include "GlutText.h"
#include "IActionListener.h"

using namespace InGE;
/**
	@author 
*/
class GameLoader : public IActionListener {
private:
	SetupManager *m_pSetupManager;
	EngineLoader *m_pEngineLoader;
	int m_lastSpawn;
	int m_team;
	int m_typeOfHost;
	bool m_state;
	bool m_menu;
	int m_lastLife, m_lastBalance;
			
	AvatarWATC *m_pAvatar;
	
	GlutMenuItem *m_itemServer;
	GlutMenuItem *m_itemClient;
	GlutMenuItem *m_itemExit;
	GlutMenuItem *m_itemChampignon;
	GlutMenuItem *m_itemOlive;
	GlutLabel *m_label;
	GlutLabel *m_score1;
	GlutLabel *m_score2;
	GlutText *m_text;
	
	vector<TextureDecorator *> m_vetLifeDisplay;
	vector<TextureDecorator *> m_vetChampScore;
	vector<TextureDecorator *> m_vetOliveScore;
	string m_team1Score;
	string m_team2Score;
		
	void loadTex();
	
public:
	GameLoader(SetupManager *pSetupManager, EngineLoader *pEngineLoader);

	~GameLoader();
	
	void actionPerformed(GlutLabel *obj);
	void escondeMenu();

	int getTeam();
	int getTypeOfHost();
	bool getState();
	
	void loadScreen(string screenName,string texFile);
	void unloadScreen(string screenName);
	
	void moviesIntro();
	void mainMenu();
	void initGame();
	
	Vector3 getSpawnPoint();
	
	void updateScore(int champignonsScore,int olivesScore);
	void updateLife();
	
};

#endif
