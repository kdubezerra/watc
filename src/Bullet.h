//
// C++ Interface: Bullet
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef BULLET_H
#define BULLET_H

#include <InGE.h>
#include "AvatarWATC.h"

using namespace InGE;

/**
	@author Rodrigo
*/
class Bullet : public Object3D {
	
	private:
		
		int m_defaultBulletDamage;
	
	public:
// 	Bullet(PhysicalGeom &geom) : ITrigger(geom), IEntity(string("info_bullet"));
	Bullet(PhysicalGeom *geom=NULL, int defaultDamageAmount=10);
	~Bullet();
	
	//virtual void doAction(PhysicalGeom &geom);
	
	/**
	 * 
	 * Os metodos abaixo nao estao mais sendo usados no jogo
	 * 
	 */
// 	void onObjectCollision(Object3D* object3d = NULL, PhysicalContactPoint *contacts = NULL , unsigned int numOfContactPoints=0);
// 	void worldDynamicWithCollision(PhysicalContactPoint *moveData = NULL);
// 	int getRealDamage();
// 	void defaultBulletDamageToZero();
};

#endif
