#ifndef GLUTLABEL_H
#define GLUTLABEL_H

#include <InGE.h>
#include "IActionListener.h"
#include <vector>


using namespace InGE;
using namespace std;

// TODO: pegar implementacao do AirroX
class GlutLabel : public IWidget {
protected:
	void *m_font;
	char m_text[256];
	vector<IActionListener *> m_listeners;
	float m_color[3];
	bool m_visible;

public:
	GlutLabel(char *text);

	virtual void setText(char *text);
 	virtual void setText(string text);
	virtual char *getText() { return m_text; }

	virtual void setVisible(bool visible) { m_visible = visible; }
	virtual bool isVisible() { return m_visible; }

	virtual void addActionListener(IActionListener *listener);
	virtual void removeActionListener(IActionListener *listener);
	virtual void fireAction(GlutLabel *obj);

	virtual void draw(float x, float y, float width, float height);

};

#endif
