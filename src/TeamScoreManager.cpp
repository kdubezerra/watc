//
// C++ Implementation: TeamScoreManager
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "TeamScoreManager.h"
#include "AvatarWATC.h"

TeamScoreManager::TeamScoreManager(int type)
{
	m_managerType = type;
	
	m_champignonsScore = 0;
	m_olivesScore = 0;
	
	if (m_managerType == SERVER) {
	}
	
	if (m_managerType == CLIENT) {
		requireScore();		
	}
}


TeamScoreManager::~TeamScoreManager()
{
}


void TeamScoreManager::check() {
	NetClient* pNetClient = NetClient::getInstance();
	NetCustomMessage* customMessage;
	
	customMessage = pNetClient->nextCustomMessage();
	
	if (customMessage == NULL)
		return;
	
	string msgType;
	
	customMessage->getAttribute("CUS_MSG_TYPE" , msgType);
		
	if (msgType == "DEATH" && m_managerType == SERVER)
		handleDeath(customMessage);//ATUALIZA SCORE E AVISA Todo MUNDO
	
	if (msgType == "SCORE_REQ" && m_managerType == SERVER)
		broadCastScore();//responde a algu� que acabou de pedir o score atual
	
	if (msgType == "SCORE_CURRENT" && m_managerType == CLIENT)
		updateScores(customMessage);

	if (msgType == "SOUND") // FIXME armengue! isto nao deveria estar aqui
		playSound(customMessage);
	
	if (customMessage) delete (customMessage);
}


void TeamScoreManager::requireScore() {
	NetCustomMessage* requireMsg = new NetCustomMessage();
	NetClient* pNetClient = NetClient::getInstance();
	
	requireMsg->setAttribute( "CUS_MSG_TYPE" , "SCORE_REQ" );
	
	pNetClient->sendCustomMessage( requireMsg );
	
	if (requireMsg) delete (requireMsg);
}


void TeamScoreManager::handleDeath(NetCustomMessage* deathMsg) {
	if (!deathMsg) return;
	
	string team;
	
	deathMsg->getAttribute ( "TEAM" , team );
	
	if (team == "CHAMPIGNONS")
		m_olivesScore++;
	 
	if (team == "OLIVES")
		m_champignonsScore++;
	
	broadCastScore();
}

void TeamScoreManager::playSound(NetCustomMessage* soundMsg) {
	if (!soundMsg) return;
	
	int number;
	string id;
	string buffernum("BUFFER_NUM");
	soundMsg->getAttribute ( "UNIQUEID" , id ); // funciona?
	soundMsg->getAttribute ( buffernum , number );
	
// 	id.erase(id.size()-1, 1);

	AvatarWATC *avatar = (AvatarWATC *)EntityFactory::getInstance()->getEntity(id);
	if (avatar)
		avatar->playSound(number);
	else
		cout << "Avatar nulo: " << id << endl;
}


void TeamScoreManager::broadCastScore() {
	NetCustomMessage* scoreMsg = new NetCustomMessage();
	NetClient* pNetClient = NetClient::getInstance();
	
	scoreMsg->setAttribute( "CUS_MSG_TYPE" , "SCORE_CURRENT" );
	scoreMsg->setAttribute( "CHAMPIGNONS_SCORE" , m_champignonsScore );
	scoreMsg->setAttribute( "OLIVES_SCORE" , m_olivesScore );
	
	pNetClient->sendCustomMessage( scoreMsg );
	
	if (scoreMsg) delete (scoreMsg);
}


void TeamScoreManager::updateScores(NetCustomMessage* updateMsg) {
	if (!updateMsg) return;		
	
	updateMsg->getAttribute("CHAMPIGNONS_SCORE" , m_champignonsScore);
	updateMsg->getAttribute("OLIVES_SCORE" , m_olivesScore);
}


int TeamScoreManager::getChampignonsScore() {
	return m_champignonsScore;
}


int TeamScoreManager::getOlivesScore() {
	return m_olivesScore;
}
