//
// C++ Implementation: AudioPlayer
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "AudioPlayer.h"

AudioPlayer *AudioPlayer::m_pAudioPlayer = 0;

AudioPlayer *AudioPlayer::getInstance() {
	if (!m_pAudioPlayer)
		m_pAudioPlayer = new AudioPlayer();
	return m_pAudioPlayer;
}

AudioPlayer::AudioPlayer()
{
	AudioManager *manager = AudioManager::getInstance();

	vector<char *>filenames;
	filenames.push_back("sounds/fx/spree02.wav");//JUMP
	filenames.push_back("sounds/fx/walk01.wav");
	filenames.push_back("sounds/fx/shots/shot0101.wav");
	filenames.push_back("sounds/fx/shots/shot0102.wav");
	filenames.push_back("sounds/fx/shots/shot0103.wav");
	filenames.push_back("sounds/fx/shots/shot0104.wav");
	filenames.push_back("sounds/fx/shots/shot0105.wav");
	filenames.push_back("sounds/fx/taunt03.wav");
	filenames.push_back("sounds/fx/taunt04.wav");
	filenames.push_back("sounds/fx/taunt05.wav");
	filenames.push_back("sounds/fx/taunt06.wav");
	filenames.push_back("sounds/fx/taunt07.wav");
	filenames.push_back("sounds/fx/taunt08.wav");
	filenames.push_back("sounds/fx/taunt09.wav");
	filenames.push_back("sounds/fx/taunt10.wav");
	filenames.push_back("sounds/fx/taunt_team01.wav");
	filenames.push_back("sounds/fx/taunt_team02.wav");

	for (int i = 0; i < filenames.size(); i++) {
		AudioBuffer *buf = manager->createBuffer();
		buf->loadFile(filenames[i]);
		m_buffers.push_back(buf);
	}

	try {
		for (int i = 0; i < 32; i++) {
			m_sources.push_back(manager->createSource());
		}
	} catch (...) {
		// nao foi possivel criar o source
		// provavelmente foi criado o numero maximo de sources
		// podemos parar por aqui
	} 
}

void AudioPlayer::play(int bufferNum, Vector3& pos) {
	AudioSource *source = findStoppedSource();
	if (source) {
		source->setPosition(pos);
		source->setBuffer(*m_buffers[bufferNum]);
		source->play();
	}
}

AudioSource *AudioPlayer::findStoppedSource() {
	vector<AudioSource *>::iterator it;

	for (it = m_sources.begin(); it != m_sources.end(); it++) {
		AudioSource *source = *it;
		if (!source->isPlaying())
			return source;
	}
	
	return 0;
}

AudioPlayer::~AudioPlayer()
{
}

