#include "GlutText.h"
#include <GL/glut.h>

#define MAXLEN 250

GlutText::GlutText(char *text) : GlutLabel(text) {
}

void GlutText::draw(unsigned int x, unsigned int y, int width, int height) {
	// FIXME Armengue WARNING! Codigo duplicado com GlutLabel::draw!!!

// 	GlutLabel::draw(x, y, width, height);
// 	glutBitmapCharacter(m_font, '*');
	char *c;
	static bool flip = true;

	if (!m_visible)
		return;

	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glColor3f(m_color[0], m_color[1], m_color[2]); // Nao funciona!!
	glRasterPos2f(m_x, height - m_y + m_h + 50);

	for (c = m_text; *c != '\0'; c++)
		glutBitmapCharacter(m_font, *c);
		glutBitmapCharacter(m_font, flip ? '_': ' ');
		flip = !flip;
	
	glPopAttrib();
}

void GlutText::keyEventHandler(char c) {
	int ret = 0;
	int len = strlen(m_text);

	switch (c)
	{
		// backspace
		case 8: 
			if (len > 0) {
				len--;
				m_text[len] = '\0';
			}		
			break;
		// enter
		case '\n':
		case '\r':
			ret = 1;
			break;
		case 27: // esc
			m_text[0] = '\0';
			break;
		default:
			if (len < MAXLEN) {
				m_text[len] = c;
				m_text[len+1] = '\0';
			}
			break;
	}

	if (ret)
		fireAction(this);
}
