#ifndef IACTIONLISTENER_H
#define IACTIONLISTENER_H

#include "GlutLabel.h"

class GlutLabel;

class IActionListener {
public:
	virtual void actionPerformed(GlutLabel *obj)=0;
};

#endif
