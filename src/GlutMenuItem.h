#ifndef GLUTMENUITEM_H
#define GLUTMENUITEM_H

#include <InGE.h>
#include "GlutLabel.h"

using namespace InGE;

class GlutMenuItem : public GlutLabel, public IControlLayerListener {
protected:
	bool m_hover;
	float m_hoverColor[3];
	float m_defaultColor[3];

public:
	GlutMenuItem(char *text);
	//virtual void draw(unsigned int x, unsigned int y, int width, int height);
	virtual void controlEventHandler(ControlEnum e, ControlParam *param);
};

#endif
