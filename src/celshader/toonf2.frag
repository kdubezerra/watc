	varying vec4 diffuse,ambientGlobal, ambient;
	varying vec3 normal,lightDir,halfVector;
	varying float dist;
	uniform sampler2D tex,lightmap;
	
	
float toonalizeIntensity(float intensity){
	if (intensity > 0.75){
		intensity = 1.0;
	} else if (intensity > 0.5){
		intensity = 0.6;
	} else /*if (intensity > 0.25)*/{
		intensity = 0.3;
	}/* else{
		intensity = 0.4;
	}*/
	return intensity;
}
	
void main(){
	vec4 tex1,tex2;
	vec3 n,halfV,viewV,ldir;
	float NdotL,NdotHV;
	vec4 color = ambientGlobal;
	float att;
	
	n = normalize(normal);
	
	NdotL = max(dot(n,normalize(lightDir)),0.0);
	NdotL = toonalizeIntensity(NdotL);
	
	if (NdotL > 0.0) {
		
			att = 1.0 / (gl_LightSource[0].constantAttenuation + gl_LightSource[0].linearAttenuation * dist + gl_LightSource[0].quadraticAttenuation * dist * dist);
			color += att * (diffuse * NdotL + ambient);
	
// 			halfV = normalize(halfVector);
//			NdotHV = max(dot(n,halfV),0.0);
// 			color += att * gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV,gl_FrontMaterial.shininess);
	}
	
	tex1 = texture2D(tex,gl_TexCoord[0].st);
//  	tex2 = texture2D(lightmap,gl_TexCoord[1].st);
	
 	color = tex1 * color;
	
	color.r = float(int(color.r*20.0))/20.0;
	color.g = float(int(color.g*20.0))/20.0;
	color.b = float(int(color.b*20.0))/20.0;

	gl_FragColor = color;	
}