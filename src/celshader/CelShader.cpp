//
// C++ Implementation: CelShader
//
// Description:
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "CelShader.h"
using namespace std;
/*
CelShader::CelShader(){
	glewInit();
	if (GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader){
		cout << "Ready for GLSL" << endl;
		m_able = true;
	} else {
		cerr << "No GLSL support" << endl;
		m_able = false;
	}
}


CelShader::~CelShader(){
}

bool CelShader::turnOn(){
	setShader();

}

bool CelShader::turnOff(){

}

void CelShader::setShader() {

	char *vs = NULL,*fs = NULL,*fs2 = NULL;

	m_vertex = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	m_fragment = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	m_fragment2 = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);


	vs = textFileRead("src/celshader/toonf2.vert");
	fs = textFileRead("src/celshader/toonf2.frag");

	const char * vv = vs;
	const char * ff = fs;

	glShaderSourceARB(m_vertex, 1, &vv,NULL);
	glShaderSourceARB(m_fragment, 1, &ff,NULL);

	free(vs);free(fs);

	glCompileShaderARB(m_vertex);
	glCompileShaderARB(m_fragment);

	printInfoLog(m_vertex);
	printInfoLog(m_fragment);
	printInfoLog(m_fragment2);

	m_program = glCreateProgramObjectARB();
	glAttachObjectARB(m_program,m_vertex);
	glAttachObjectARB(m_program,m_fragment);

	glLinkProgramARB(m_program);
	printInfoLog(m_program);

	glUseProgramObjectARB(m_program);

	m_loc = glGetUniformLocationARB(m_program,"time");


}

void CelShader::printInfoLog(GLhandleARB obj){
	int infologLength = 0;
	int charsWritten  = 0;
	char *infoLog;

	glGetObjectParameterivARB(obj, GL_OBJECT_INFO_LOG_LENGTH_ARB, &infologLength);

	if (infologLength > 0){
		infoLog = (char *)malloc(infologLength);
		glGetInfoLogARB(obj, infologLength, &charsWritten, infoLog);
		printf("%s\n",infoLog);
		free(infoLog);
	}
}

char *CelShader::textFileRead(char *fn) {
	FILE *fp;
	char *content = NULL;

	int count=0;

	if (fn != NULL) {
		fp = fopen(fn,"rt");

		if (fp != NULL) {

			fseek(fp, 0, SEEK_END);
			count = ftell(fp);
			rewind(fp);

			if (count > 0) {
				content = (char *)malloc(sizeof(char) * (count+1));
				count = fread(content,sizeof(char),count,fp);
				content[count] = '\0';
			}
			fclose(fp);
		}
	}
	return content;
}
*/
