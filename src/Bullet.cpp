//
// C++ Implementation: Bullet
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "Bullet.h"

Bullet::Bullet(PhysicalGeom *geom, int defaultDamageAmount): Object3D(geom) {

	//m_defaultBulletDamage = defaultDamageAmount;  //nÃ£o esta sendo mais usado
	
	m_type = "Object3DBullet";
}

Bullet::~Bullet(){
}

/**
 * MÃ©todo para checar se a bala colidiu com um AvatarWATC, se colidiu, avisar ao avatar atingido
 * para ele reduzir seu life de acordo com o dano da bala.
 */
// void Bullet::onObjectCollision(Object3D* pObject3D, PhysicalContactPoint *contacts, unsigned int numOfContactPoints) {
	
// 	cout << "############# entrando no metodo on collision da bullet ##########" << endl;
// 	
// 	cout << "$$$$$$$$$$$$ colidi com um : " << pObject3D->getType() << " $$$$$$$$$$$" << endl;
// 	
// 	if (pObject3D && (pObject3D->getType() == "AvatarWATC")) {
// 		
// 		cout << "acertei um avatarWATC" << endl;
// 		
// 		AvatarWATC* pAvatarWATC = (AvatarWATC *) pObject3D;
// 		
// 		pAvatarWATC->takeDamage(getRealDamage());
// 	}
// 	
// 	defaultBulletDamageToZero();
// }

// void Bullet::worldDynamicWithCollision(PhysicalContactPoint *moveData) {
// 	
// 	defaultBulletDamageToZero();
// }

/**
 * MÃ©todo para retornar o dano da bala, criado para possibilidar a existÃªncia de danos aleatÃ³rios 
 * baseados no dano padrÃ£o da bala.
 */
// int Bullet::getRealDamage() {
// 	
// 	return m_defaultBulletDamage;
// }

// void Bullet::defaultBulletDamageToZero() {
// 	
// 	if (m_defaultBulletDamage) {
// 		
// 		m_defaultBulletDamage = 0;
// 		cout << "Sou uma bullet, atingi alguÃ©m e meu dano agora eh: " << m_defaultBulletDamage << endl;
// 	}	
// }

