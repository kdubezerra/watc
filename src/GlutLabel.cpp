#include "GlutLabel.h"
#include <GL/glut.h>

GlutLabel::GlutLabel(char *text) {
	m_font = GLUT_BITMAP_TIMES_ROMAN_24;
	setText(text);

	m_color[0] = 1.0;
	m_color[1] = 0.0;
	m_color[2] = 0.0;

	m_visible = true;
}

void GlutLabel::setText(char *text) {
	strcpy(m_text, text);

	m_w = 0;
	char *c;
	for (c = m_text; *c != '\0'; c++)
		m_w += glutBitmapWidth(m_font, *c);

	m_h = 28; //m_h = glutBitmapHeight(m_font); // FIXME Armengue!
}

void GlutLabel::setText(string text) {
	strcpy(m_text, text.c_str());

	m_w = 0;
	char *c;
	for (c = m_text; *c != '\0'; c++)
		m_w += glutBitmapWidth(m_font, *c);

	m_h = 28; //m_h = glutBitmapHeight(m_font); // FIXME Armengue!
}


void GlutLabel::addActionListener(IActionListener *listener) {
	m_listeners.push_back(listener);
}

void GlutLabel::removeActionListener(IActionListener *listener) {
	vector<IActionListener *>::iterator it;
	
	for(it = m_listeners.begin(); it != m_listeners.end(); it++){
		if(listener == *it)
			m_listeners.erase(it);
	}
}

void GlutLabel::fireAction(GlutLabel *obj) {
	vector<IActionListener *>::iterator it;
	for(it = m_listeners.begin(); it != m_listeners.end(); it++)
		(*it)->actionPerformed(obj);
}

void GlutLabel::draw(float x, float y, float width, float height) {
	char *c;

	if (!m_visible)
		return;	

	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glColor3f(m_color[0], m_color[1], m_color[2]); // Nao funciona!!	
	glRasterPos2f(m_x, m_y + m_h);

	for (c = m_text; *c != '\0'; c++)
		glutBitmapCharacter(m_font, *c);
	
	glPopAttrib();
}

