//
// C++ Implementation: AvatarWATC
//
// Description:
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "AvatarWATC.h"
#include "AudioPlayer.h"


AvatarWATC::AvatarWATC(PhysicalGeom *geom, int life) : Avatar(geom){
  m_type = "AvatarWATC";
  m_lastWalkSoundTime = 0;
  m_bulletSpeed = 750;
  //	m_pThreadBulletRemover = SDL_CreateThread( bulletRemover , this );
  m_termThread = false;
  m_life = life; //life do avatar
  m_dead = false;
  m_respawn = false;
  m_walking = false;
  m_keysPressed = 0;
  last_idle = "IDLE1";

  vector<char *>filenames;
  filenames.push_back("sounds/fx/spree02.wav");//JUMP
  filenames.push_back("sounds/fx/walk01.wav");
  filenames.push_back("sounds/fx/shots/shot0101.wav");
  filenames.push_back("sounds/fx/shots/shot0102.wav");
  filenames.push_back("sounds/fx/shots/shot0103.wav");
  filenames.push_back("sounds/fx/shots/shot0104.wav");
  filenames.push_back("sounds/fx/shots/shot0105.wav");
  filenames.push_back("sounds/fx/taunt03.wav");
  filenames.push_back("sounds/fx/taunt04.wav");
  filenames.push_back("sounds/fx/taunt05.wav");
  filenames.push_back("sounds/fx/taunt06.wav");
  filenames.push_back("sounds/fx/taunt07.wav");
  filenames.push_back("sounds/fx/taunt08.wav");
  filenames.push_back("sounds/fx/taunt09.wav");
  filenames.push_back("sounds/fx/taunt10.wav");
  filenames.push_back("sounds/fx/taunt_team01.wav");
  filenames.push_back("sounds/fx/taunt_team02.wav");

  AudioManager *manager = AudioManager::getInstance();

  AudioBuffer *buf = manager->createBuffer();
  for (int i = 0; i < filenames.size(); i++) {
    buf = manager->createBuffer();
    buf->loadFile(filenames[i]);
    AudioSource *source = manager->createSource();
    source->setBuffer(*buf);
    m_sources.push_back(source);
  }

}


AvatarWATC::~AvatarWATC()
{}

int AvatarWATC::getLife(){

  return m_life;
}

void AvatarWATC::controlEventHelper(bool &moviment, bool &movimentOpposed, int &keysPressed, int eventCode, int eventCodeOpposed, bool paramState){
	if(paramState) {
		moviment = true;
		movimentOpposed = false;
		keysPressed |= eventCode;
	} else {
		moviment = false;
		keysPressed &= ~eventCode;
		if ((keysPressed | eventCodeOpposed) == keysPressed) {
			movimentOpposed = true;
		}
	}
}

/**
 * Manipula as aÃ§Ãµes dos controles
 * @param ControlEnum e - EnumeraÃ§Ã£o dos comandos.
 * @param ControlParam *param - Parametros do comando passado.
 */
void AvatarWATC::controlEventHandler(ControlEnum e, ControlParam *param){
	switch(e)	{
	case InGE_CONTROL_FORWARD:
		controlEventHelper(m_movimentState.forward, m_movimentState.backward, m_keysPressed, WATC_FORWARD, WATC_BACKWARD, param->state);
		break;
	case InGE_CONTROL_BACKWARD:
		controlEventHelper(m_movimentState.backward, m_movimentState.forward, m_keysPressed, WATC_BACKWARD, WATC_FORWARD, param->state);
		break;
	case InGE_CONTROL_LEFT:
		controlEventHelper(m_movimentState.strafeLeft, m_movimentState.strafeRight, m_keysPressed, WATC_STRAFE_LEFT, WATC_STRAFE_RIGHT, param->state);
		break;
	case InGE_CONTROL_RIGHT:
		controlEventHelper(m_movimentState.strafeRight, m_movimentState.strafeLeft, m_keysPressed, WATC_STRAFE_RIGHT, WATC_STRAFE_LEFT, param->state);
		break;
	case InGE_CONTROL_TURN_LEFT:
		controlEventHelper(m_movimentState.turnLeft, m_movimentState.turnRight, m_keysPressed, WATC_TURN_LEFT, WATC_TURN_RIGHT, param->state);
		break;
	case InGE_CONTROL_TURN_RIGHT:
		controlEventHelper(m_movimentState.turnRight, m_movimentState.turnLeft, m_keysPressed, WATC_TURN_RIGHT, WATC_TURN_LEFT, param->state);
		break;
	case InGE_CONTROL_JUMP:
		if(param->state) {
			m_movimentState.jump = true;
		}
		break;
	case InGE_CONTROL_ATTACK1:
		if(param->state && weaponCanFire(SDL_GetTicks()) ) {
			if (!m_dead){
				fire();
			} else {
				respaw();
			}
		}
		break;
	case InGE_CONTROL_LOOK:
		m_pointerX = param->x;
		m_pointerY = param->y;
		break;
	default:
		break;
	}
}

void AvatarWATC::fire(){
	Bullet *bullet = new Bullet();
	EntityFactory::getInstance()->addObject3D(bullet);
	
	IModel *pModel = KFModelFactory::getInstance()->loadMd3("models/ketchup");
	if(pModel){
		bullet->setModel(pModel);
	}
	
	Vector3 fireposition = getPosition();
	fireposition[0] += getDirection()[0]*28;
	fireposition[1] += getDirection()[1]*28;
	
	
	
	bullet->setPosition(fireposition);
	bullet->setVelocity(m_direction*m_bulletSpeed);
	bullet->setDirection(m_direction);
	((GeomSphere *)bullet->pGetPhysicalGeom() )->setRadius(5);
	
	m_qBulletCreationInstant.push(SDL_GetTicks());
	m_qBulletInstance.push(bullet);
	
	m_lastFireTime = SDL_GetTicks();
	
	if (m_pModel){
		if (m_pModel->getAnimation(m_id) == "WALK"){
			m_pModel->setAnimation(m_id, "FIRE", "WALK");
		} else {
			m_pModel->setAnimation(m_id, "FIRE", pModel->getAnimation(m_id));
		}
	}
	
	fireSound();
}

void AvatarWATC::fireSound(){
	int i = rand();
	i %= 5;
	i += 2;
	sendSound(i);
}


void AvatarWATC::sendSound(int number) {
	NetCustomMessage* soundMsg = new NetCustomMessage();
	NetClient* pNetClient = NetClient::getInstance();
	
	soundMsg->setAttribute("CUS_MSG_TYPE" , "SOUND");
	soundMsg->setAttribute("UNIQUEID", m_id);
	soundMsg->setAttribute("BUFFER_NUM", number);
	
	pNetClient->sendCustomMessage(soundMsg , false);
	
	if (soundMsg)
		delete (soundMsg);

	playSound(number);
}

void AvatarWATC::playSound(int number) {
	if (number < m_sources.size()) {
		Vector3 pos = getPosition();
		AudioPlayer::getInstance()->play(number, pos);
	}
		//m_sources[number]->play();
}

/**
 * MÃ©todo para verificar se a arma pode disparar num dado momento.
 * @param float currentTime - Momento em que se tenta disparar a arma.
 * @return boolean - retorna true se a diferenÃ§a entre o currentTime e o m_lastFireTime for maior ou igual Ã  velociade de disparo da arma
 */
bool AvatarWATC::weaponCanFire(float currentTime){

  float elapsedTime = currentTime - m_lastFireTime;
  const float weaponSpeed = 400;  //velocidade de disparo padrÃ£o - magic number :P

  if (elapsedTime < weaponSpeed){
    return false;
  }

  return true;
}

int AvatarWATC::bulletRemover(void *instance){
  AvatarWATC* pAvatar = (AvatarWATC*) instance;
  EntityFactory* pEntityFactory = EntityFactory::getInstance();

  while( !pAvatar->m_termThread ){
    SDL_Delay(500);

    NetControl::waitForSync();

    if (!pAvatar->m_qBulletInstance.empty() && SDL_GetTicks() > pAvatar->m_qBulletCreationInstant.front() + M_BULLET_TIMEOUT) {
      pEntityFactory->rmEntity(pAvatar->m_qBulletInstance.front());
      pAvatar->m_qBulletInstance.pop();
      pAvatar->m_qBulletCreationInstant.pop();
    }

    NetControl::postForSync();

  }

}

/**
 * Diminui uma quantidade de dano do life atual
 * @param damageAmount - quantidade de dano a ser reduzida do life
 */
void AvatarWATC::takeDamage( int damageAmount){
	m_life -= damageAmount;
	m_pModel->setAnimation(m_id, "HURT", idle());
	if (m_life == 0){
		if (!m_dead){
			m_dead = true;
			m_deadTime = SDL_GetTicks();
			if (m_pModel){
				m_pModel->setAnimation(m_id, "DEATH", "DEAD");
			}
			warnOwnDeath();
		}
  }else if((m_life < 0) && ( (m_pModel->getAnimation(m_id) != "DEAD") || (m_pModel->getAnimation(m_id) != "DEATH"))){
	  m_life = 0;
		if (m_pModel){
			m_pModel->setAnimation(m_id, "DEAD", "DEAD");
		}
  }
}

void AvatarWATC::update(){
	if(!m_dead){
		bool jump = m_movimentState.jump;
		Avatar::update();

		if((jump && (m_walking == true)) || (jump && m_worldDynamicCollided) ){
			performJump();
		}
		
		if(m_movimentState.forward || m_movimentState.backward || m_movimentState.strafeLeft || m_movimentState.strafeRight ){
			performWalk();
		} else {
			m_walking = false;
			if ((m_pModel) && (m_pModel->getAnimation(m_id) == "WALK")){
				cerr << "Setando IDLE" << endl;
				m_pModel->setAnimation(m_id, idle(), idle());
			}
		}
	} else if (m_movimentState.forward || m_movimentState.backward || m_movimentState.strafeLeft || m_movimentState.strafeRight || m_movimentState.jump){
		m_movimentState.forward = false;
		m_movimentState.backward = false;
		m_movimentState.strafeLeft = false;
		m_movimentState.strafeRight = false;
		m_movimentState.jump = false;
		Avatar::update();
		respaw();
		
	} else {
		Avatar::update();
	}
}	

/**
 * 
 */
void AvatarWATC::performWalk(){
	float currentTime = SDL_GetTicks();
	float elapsedTime = currentTime - m_lastWalkSoundTime;
	const float delay = 450;  //velocidade de disparo padrÃ£o - magic number :P
	if ((m_worldDynamicCollided)){
		m_walking = true;
		if (elapsedTime > delay){
			sendSound(1);
			m_lastWalkSoundTime = currentTime;
			Vector3 littleJump = this->getVelocity();
			littleJump[2] += 40;
			this->setVelocity(littleJump);
		}
		if ((m_pModel) && (m_pModel->getAnimation(m_id) != "WALK")){
			m_pModel->setAnimation(m_id, "WALK", "WALK");
		}
	}
}

/**
 * 
 */
void AvatarWATC::performJump(){
	if (m_sources[2]) { // FIXME remover: teste de audio
		sendSound(2);
	}
	if (m_pModel)  {
		m_pModel->setAnimation(m_id, "JUMP", idle());
	}
	Vector3 v(0,0, m_jumpSpeed);
	this->setVelocity(v);
	m_movimentState.jump = false;
	m_walking = false;
}

/**
 * Checa se o avatar colidiu com alguma bullet maldita, se sim remove a bala do jogo
 */
void AvatarWATC::onObjectCollision(Object3D* object3d, PhysicalContactPoint *contacts, unsigned int numOfContactPoints) {
	if (!object3d) return;
	
	if (object3d->getType() == "Object3D") {
		if (!object3d->isObjectCollided() && !object3d->isWorldDynamicCollided()) {
		
			takeDamage(M_BULLET_DAMAGE);
		}
	}
}

void AvatarWATC::worldTargetWithCollision(PhysicalContactPoint *moveData){
  Avatar::worldTargetWithCollision(moveData);
}

void AvatarWATC::worldDynamicWithCollision(PhysicalContactPoint *moveData){
  Avatar::worldDynamicWithCollision(moveData);
}

void AvatarWATC::worldTargetWithoutCollision(PhysicalContactPoint *moveData){
  Avatar::worldTargetWithoutCollision(moveData);
}

void AvatarWATC::worldDynamicWithoutCollision(PhysicalContactPoint *moveData){
  Avatar::worldDynamicWithoutCollision(moveData);
}

void AvatarWATC::checkStuff(){
/*  checkOwnLife();*/
  checkDeadBullets();
}

void AvatarWATC::checkOwnLife(){
/*  if (!m_dead) {
    if(m_life <= 0) {
      m_dead = true;
      warnOwnDeath();
    }
  }

  if (m_dead){
    if (m_life > 0){
      m_dead = false;
    }
  }*/
}

void AvatarWATC::warnOwnDeath() {
	NetCustomMessage* deathMsg = new NetCustomMessage();
	NetClient* pNetClient = NetClient::getInstance();
	
	deathMsg->setAttribute("CUS_MSG_TYPE" , "DEATH");
	if (m_team == 0)
		deathMsg->setAttribute("TEAM" , "CHAMPIGNONS");
	else
		deathMsg->setAttribute("TEAM" , "OLIVES");
	
	pNetClient->sendCustomMessage(deathMsg , true);
	
	if (deathMsg)
		delete (deathMsg);
}

void AvatarWATC::checkDeadBullets(){
  EntityFactory* pEntityFactory = EntityFactory::getInstance();

  if (m_qBulletInstance.empty())
    return;

  if (SDL_GetTicks() > m_qBulletCreationInstant.front() + M_BULLET_TIMEOUT) {
    pEntityFactory->rmEntity(m_qBulletInstance.front());
    m_qBulletInstance.pop();
    m_qBulletCreationInstant.pop();
  }

}

void AvatarWATC::setTeam(int team) {
	m_team = team;
}

int AvatarWATC::getTeam() {
	return m_team;
}

bool AvatarWATC::getRespawn() {
	return m_respawn;
}

void AvatarWATC::setRespawn(bool respawn) {
	m_respawn = respawn;
}

void AvatarWATC::setModel(IModel *pModel){
	if (!pModel) return;
	
	Object3D::setModel(pModel);
	
	m_pModel->setDraw(m_id, false, m_id);
	
	m_pModel->setAnimation(m_id, last_idle, idle());
}

string AvatarWATC::idle() {
  string idles[] = {"IDLE1", "IDLE2", "IDLE3"};
  last_idle = idles[SDL_GetTicks() % 3];
  return last_idle;
}

void AvatarWATC::draw( ){
	if (m_pModel){
		Drawer *drawer = Drawer::getInstance();
		
		Vector3 position(getPosition());
		
		drawer->translate( position );
		
		float angle = getAngleRotation()+180;
		
		drawer->rotate( angle,0,0,1);
		
		m_pModel->drawModel( m_id );
		
		drawer->rotate( angle,0,0,-1);
		
		position *= -1;
		drawer->translate( position );
	}	
}

void AvatarWATC::respaw( ){
	if (m_dead){
		float currentTime = SDL_GetTicks();
		if ( (currentTime - m_deadTime) >= 10000 ){
			m_respawn = true;
			m_life = 8;
			m_dead = false;
			if (m_pModel){
				m_pModel->setAnimation(m_id, last_idle, idle());
			}
		}
	}
}
