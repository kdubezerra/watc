//
// C++ Implementation: GameLoader
//
// Description: 
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "GameLoader.h"

GameLoader::GameLoader(SetupManager *setupManager, EngineLoader *pEngineLoader){
	m_pSetupManager = setupManager;
	m_pEngineLoader = pEngineLoader;
	
	m_lastSpawn = 0;
	m_team = 0;
	m_state = true;
	m_lastLife = 0;
	m_lastBalance = 0;
	
	m_score1 = NULL;
	m_score2 = NULL;
				
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	loadTex();
	SceneManager *sceneManager = SceneManager::getInstance();
	sceneManager->getRenderManager()->addWidget(m_vetChampScore[1]);
	sceneManager->getRenderManager()->addWidget(m_vetOliveScore[1]);
}

void GameLoader::loadTex(){
	for(char i = '0'; i < '9'; i++){
		Window *status = new Window(0,0,128,128);
		string file = "textures/GUI/life";
		file += i;
		
		TextureDecorator *td = new TextureDecorator(file, status);
		td->setName(file);
// 		td->move(10.0,450.0);
		td->move(10.0,10.0);
		m_vetLifeDisplay.push_back(td);
	}
	for(char i = '0'; i < '3'; i++){
		Window *status = new Window(0,0,128,128);
		string file = "textures/GUI/champ";
		file += i;
		
		TextureDecorator *td = new TextureDecorator(file, status);
		td->setName(file);
// 		td->move(600.0,500.0);
		td->move(600.0,5.0);
		m_vetChampScore.push_back(td);
	}
	for(char i = '0'; i < '3'; i++){
		Window *status = new Window(0,0,128,128);
		string file = "textures/GUI/olive";
		file += i;
		
		TextureDecorator *td = new TextureDecorator(file, status);
		td->setName(file);
		td->move(600.0,105.0);
		m_vetOliveScore.push_back(td);
	}
}


GameLoader::~GameLoader(){
	if (m_itemServer) delete m_itemServer;
	if (m_itemClient) delete m_itemClient;
	if (m_itemExit) delete m_itemExit;
	if (m_label) delete m_label;
	if (m_text) delete m_text;
	if (m_itemChampignon) delete m_itemChampignon;
	if (m_itemOlive) delete m_itemOlive;
	if (m_score1) delete m_score1;
	if (m_score2) delete m_score2;
	
}

void GameLoader::escondeMenu() {
	m_itemClient->setVisible(false);
	m_itemServer->setVisible(false);
	m_itemExit->setVisible(false);
	m_label->setVisible(false);
	m_text->setVisible(false);
	
	// ta dando memory corruption
// 	m_itemClient->removeActionListener(this);
// 	m_itemServer->removeActionListener(this);
// 	m_itemExit->removeActionListener(this);
}

void GameLoader::actionPerformed(GlutLabel *obj) {
	static int opcao1 = 0;
	static int opcao2 = 0;
	static int subopcao = 0;
	static char ip[50];

	switch (opcao1) {
		case 0: // Estamos no menu
			if (obj == m_itemExit) {
				opcao1 = 3;
				SceneManager::getInstance()->setState(false);
				m_state = false;
				m_menu = false;
			}
			else if (obj == m_itemServer) {
				m_typeOfHost = opcao1 = 1;
				subopcao = 1;
				escondeMenu();
				m_label->setText("Digite um cenario: ");
				m_text->setText("maps/pizza.bsp");
				m_label->setVisible(true);
				m_text->setVisible(true);
			}
			else if (obj == m_itemClient) {
				m_typeOfHost = opcao1 = 2;
				subopcao = 1;
				escondeMenu();
				m_label->setText("Digite o IP do servidor: ");
				m_text->setText("192.168.136.4");
				m_label->setVisible(true);
				m_text->setVisible(true);
				
				
				// TODO: pede nick
			}
			break;
		case 1: // Estamos na opcao Servidor
			if (obj == m_text) {
				if (subopcao == 1) { // cenario
					subopcao = 2;
					m_pSetupManager->setServer(15000, m_text->getText());
					m_label->setText("Digite seu nick: ");
					m_text->setText("");
					
					m_label->setVisible( false );
					m_text->setVisible( false );
					
					m_itemChampignon->setVisible( true );
					m_itemOlive->setVisible( true );
				}
			}
			if (subopcao == 2) { // team
				if (obj == m_itemChampignon){
					m_team = 0;
					subopcao = 3;
					m_itemChampignon->setVisible( false );
					m_itemOlive->setVisible( false );
					
				} else if (obj == m_itemOlive){
					m_team = 1;
					subopcao = 3;
					m_itemChampignon->setVisible( false );
					m_itemOlive->setVisible( false );
					
				}
			}	else if (subopcao == 3) { // nick
				subopcao = 3;
				m_pSetupManager->setClient(m_text->getText(), "localhost",15000);
				escondeMenu();
				m_menu = false;
			}
			
			break;
		case 2: // Estamos na opcao Cliente
			if (obj == m_text) {
				if (subopcao == 1) { // ip
					subopcao = 2;
					strcpy(ip, m_text->getText());
					m_label->setText("Digite seu nick: ");
					m_text->setText("");
					
					m_label->setVisible( false );
					m_text->setVisible( false );
					
					m_itemChampignon->setVisible( true );
					m_itemOlive->setVisible( true );
				}
			}
			if (subopcao == 2) { // team
				if (obj == m_itemChampignon){
					m_team = 0;
					subopcao = 3;
					m_itemChampignon->setVisible( false );
					m_itemOlive->setVisible( false );
					
				} else if (obj == m_itemOlive){
					m_team = 1;
					subopcao = 3;
					
					m_itemChampignon->setVisible( false );
					m_itemOlive->setVisible( false );
				}
			} else if (subopcao == 3) { // nick
				m_pSetupManager->setClient(m_text->getText(), ip,15000);
				m_menu = false;
			}
			break;
		case 3: //Saindo 
			SceneManager::getInstance()->setState(false);
			m_state = false;
			break;
			break;
			
	}
}

void GameLoader::mainMenu(){
	ControlLayer *controlLayer = m_pEngineLoader->getControlLayer();
	controlLayer->unsetMouseOnCenter();
	controlLayer->grabOff();
	
	SceneManager *pSceneManager = SceneManager::getInstance();
	
	
	loadScreen("menu","textures/screen.tga");
	CameraFP *pCamera = new CameraFP();
	pCamera->setPosition(Vector3(0,0,100));
	pCamera->setViewPoint(Vector3(0,0,0));
	pCamera->setUp(Vector3(0,-1,0));
	

	// GUI
	m_itemServer = new GlutMenuItem("Novo jogo modo servidor");
	m_itemClient = new GlutMenuItem("Conectar-se a um servidor");
	m_itemExit = new GlutMenuItem("Sair");
	
	m_itemChampignon = new GlutMenuItem("Champignons");;
	m_itemOlive = new GlutMenuItem("Azeitonas");;



	m_label = new GlutLabel("");
	m_text = new GlutText("");
	
	m_label->move(10, 20);
	m_label->setVisible(false);
	m_text->move(10, 50);
	m_text->setVisible(false);
	m_pEngineLoader->getControlLayer()->getKeyboardControl()->addKeyListener(m_text);
	
	m_itemChampignon->setVisible( false );
	m_itemOlive->setVisible( false );
	
	
	Window *window = new Window(10, 10, 200, 500);
 	
	m_itemServer->move(10, 60);
	m_itemClient->move(10, 100);
	m_itemExit->move(10, 140);
	m_itemChampignon->move(250,100);
	m_itemOlive->move(450,100);
		
	controlLayer->addListener(m_itemServer);
	controlLayer->addListener(m_itemClient);
	controlLayer->addListener(m_itemExit);
	controlLayer->addListener(m_itemChampignon);
	controlLayer->addListener(m_itemOlive);
		
	m_itemServer->addActionListener(this);
	m_itemClient->addActionListener(this);
	m_itemExit->addActionListener(this);
	m_text->addActionListener(this);
	m_itemChampignon->addActionListener(this);
	m_itemOlive->addActionListener(this);
	
	
	window->add(m_itemServer);
	window->add(m_itemClient);
	window->add(m_itemExit);
	window->add(m_label);
	window->add(m_text);
	window->add(m_itemChampignon);
	window->add(m_itemOlive);
		
 	pSceneManager->getRenderManager()->addWidget(window);

	
	m_menu = true;
	while (m_menu){
		controlLayer->check();
		pSceneManager->getRenderManager()->render(pCamera);
		
	}
	unloadScreen("menu");
	
	controlLayer->removeListener(m_itemServer);
	controlLayer->removeListener(m_itemClient);
	controlLayer->removeListener(m_itemExit);
	m_pEngineLoader->getControlLayer()->getKeyboardControl()->removeKeyListener(m_text);
	
}

/**
 * 
 */
void GameLoader::initGame(){
	EntityFactory *pEntityFactory = EntityFactory::getInstance();
	ControlLayer *controlLayer = m_pEngineLoader->getControlLayer();
	controlLayer->setMouseOnCenter();
// 	controlLayer->grabOn();

	
	NetClient *netClient = m_pSetupManager->getNetClient();
	m_pEngineLoader->createScene(netClient->getScene());
	
	AvatarWATC* pAvatar = new AvatarWATC();
	m_pAvatar = pAvatar;
	if(m_team == 0){
		m_pEngineLoader->createPlayer( "models/champignon" , pAvatar);
	} else {
		m_pEngineLoader->createPlayer( "models/azeitona" , pAvatar);//FIXME:Mudar para time da azeitona
	}
	m_pAvatar->setTeam(m_team);
	
	Vector3 pos = getSpawnPoint();
	m_pAvatar->setPosition( pos );
	
	m_pEngineLoader->loadStream("sounds/music/champignons.ogg");
	m_pEngineLoader->getPlayer()->getStream()->setGain(0.3);
	m_pEngineLoader->getPlayer()->getStream()->setLooping(true);
	
	netClient->startReceiver();
	netClient->startSyncSender();

	Window *window = new Window(10, 10, 200, 500);
	m_label = new GlutLabel("");
	m_score1 = new GlutLabel("");
	m_score2 = new GlutLabel("");
	
	
	m_score1->move(700,50);
	m_score2->move(700,150);
	window->add(m_label);
	window->add(m_score1);
	window->add(m_score2);
	
	SceneManager *pSceneManager = SceneManager::getInstance();
	pSceneManager->getRenderManager()->addWidget(window);
	
}

bool GameLoader::getState(){
	return m_state;
}

void GameLoader::moviesIntro(){
	loadScreen("movie","movies/watc.mpg");
	
	ControlLayer *controlLayer = m_pEngineLoader->getControlLayer();
	controlLayer->unsetMouseOnCenter();
	controlLayer->grabOff();
	
	SceneManager *pSceneManager = SceneManager::getInstance();

	
	CameraFP *pCamera = new CameraFP();
	pCamera->setPosition(Vector3(0,0,100));
	pCamera->setViewPoint(Vector3(0,0,0));
	pCamera->setUp(Vector3(0,-1,0));
	
	while (pSceneManager->getState()){
		controlLayer->check();
		pSceneManager->getRenderManager()->render(pCamera);
 		
	}
	pSceneManager->setState(true);
	unloadScreen("movie");
}

void GameLoader::loadScreen(string screenName, string texFile){
	SDL_Surface *surface = SDL_GetVideoSurface();	
	int w = surface->w;
	int h = surface->h;

	Window *status = new Window(0,0,w,h);
	
	TextureDecorator *td = new TextureDecorator(texFile, status);
	td->setName(screenName);
	
	SceneManager *sceneManager = SceneManager::getInstance();
	sceneManager->getRenderManager()->addWidget(td);
}

void GameLoader::unloadScreen(string screenName){
	SceneManager *sceneManager = SceneManager::getInstance();
	IWidget *pWidget = sceneManager->getRenderManager()->pGetWidget(screenName);
	sceneManager->getRenderManager()->rmWidget(screenName);
	delete pWidget;
}

int GameLoader::getTeam() {
	return m_pAvatar->getTeam();
}

int GameLoader::getTypeOfHost() {
	return m_typeOfHost;
}

void GameLoader::updateScore(int champignonsScore,int olivesScore){
	int currBalance = champignonsScore - olivesScore;
	if ( (currBalance != m_lastBalance) && ( ((currBalance <= 0) && (m_lastBalance >= 0)) || ((currBalance >= 0) && (m_lastBalance <= 0))) ){
		SceneManager *sceneManager = SceneManager::getInstance();
		string name;
		if (currBalance == 0) {
			if (m_lastBalance > 0){
				name = m_vetChampScore[0]->getName();
				sceneManager->getRenderManager()->rmWidget(name);
				name = m_vetOliveScore[2]->getName();
				sceneManager->getRenderManager()->rmWidget(name);
				
				sceneManager->getRenderManager()->addWidget(m_vetChampScore[1]);
				sceneManager->getRenderManager()->addWidget(m_vetOliveScore[1]);
				
			} else {
				name = m_vetChampScore[2]->getName();
				sceneManager->getRenderManager()->rmWidget(name);
				name = m_vetOliveScore[0]->getName();
				sceneManager->getRenderManager()->rmWidget(name);
				
				sceneManager->getRenderManager()->addWidget(m_vetChampScore[1]);
				sceneManager->getRenderManager()->addWidget(m_vetOliveScore[1]);
			}
			
				
		} else if (currBalance < 0){
			name = m_vetChampScore[1]->getName();
			sceneManager->getRenderManager()->rmWidget(name);
			name = m_vetOliveScore[1]->getName();
			sceneManager->getRenderManager()->rmWidget(name);
				
			sceneManager->getRenderManager()->addWidget(m_vetChampScore[2]);
			sceneManager->getRenderManager()->addWidget(m_vetOliveScore[0]);
			
		} else {
			name = m_vetChampScore[1]->getName();
			sceneManager->getRenderManager()->rmWidget(name);
			name = m_vetOliveScore[1]->getName();
			sceneManager->getRenderManager()->rmWidget(name);
				
			sceneManager->getRenderManager()->addWidget(m_vetChampScore[0]);
			sceneManager->getRenderManager()->addWidget(m_vetOliveScore[2]);
			
		}
	}
		
	m_lastBalance = currBalance;
			
	m_score1->isVisible();
	m_score2->isVisible();
	char buffer[16];
	sprintf(buffer, "%d", champignonsScore);
	m_team1Score = buffer;
	sprintf(buffer, "%d", olivesScore);
	m_team2Score = buffer;
	m_score1->setText( m_team1Score.c_str() );
	m_score2->setText( m_team2Score.c_str() );
}


void GameLoader::updateLife(){
	int currLife = m_pAvatar->getLife();
	
	if ( m_lastLife !=  currLife){
		SceneManager *sceneManager = SceneManager::getInstance();
		
		string name = m_vetLifeDisplay[m_lastLife]->getName();
		sceneManager->getRenderManager()->rmWidget(name);
		

		sceneManager->getRenderManager()->addWidget(m_vetLifeDisplay[currLife]);
		
		m_lastLife = currLife;
	}
	
		if (m_pAvatar->getRespawn()){
			Vector3 pos( getSpawnPoint() );
			m_pAvatar->setPosition( pos );
			m_pAvatar->setRespawn( false );
		}
	
}

Vector3 GameLoader::getSpawnPoint(){
	EntityFactory *pEntityFactory = EntityFactory::getInstance();
	m_lastSpawn++;
	m_lastSpawn %= pEntityFactory->getNumInfoEntity();
	InfoEntity *infoStart = pEntityFactory->pGetInfoEntity(m_lastSpawn);
	if (infoStart){
		if (infoStart->getClassName() == "info_player_start"){
			return infoStart->getPosition();
		} else {
			return getSpawnPoint();
		}
	}
}
