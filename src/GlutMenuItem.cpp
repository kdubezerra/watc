#include "GlutMenuItem.h"
#include <GL/glut.h>

GlutMenuItem::GlutMenuItem(char *text) : GlutLabel(text) {
	m_hover = false;
	
	m_hoverColor[0] = 0.5;
	m_hoverColor[1] = 0.5;
	m_hoverColor[2] = 0.0;
	
	m_defaultColor[0] = 0.0;
	m_defaultColor[1] = 0.0;
	m_defaultColor[2] = 0.0;
}

void GlutMenuItem::controlEventHandler(ControlEnum e, ControlParam *param) {
	int x = param->x;
	int y = param->y;
	bool click = (e == InGE_CONTROL_ATTACK1);

	m_hover = (x >= m_x && x < m_x + m_w && y >= m_y && y < m_y + m_h);

	// troca a cor
	float *color = (m_hover) ? m_hoverColor : m_defaultColor;
	m_color[0] = color[0];
	m_color[1] = color[1];
	m_color[2] = color[2];

// 	char buf[256];
// 	sprintf(buf, "hover=%d, click=%d\n", m_hover, click);
// 	this->setText(buf);
	if (m_hover && click)
		fireAction(this);
}
