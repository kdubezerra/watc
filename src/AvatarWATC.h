//
// C++ Interface: AvatarWATC
//
// Description: Avatar para o jogo We Are The Champignons
//
//
// Author: Cicero Augusto <ciceroaugusto@gmail.com>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef AVATAR_H
#define AVATAR_H

#include <InGE.h>
#include "GlutLabel.h"
#include <string>

#include "Bullet.h"

using namespace InGE;
/**
@author Cicero Augusto M. da S. Neves <ciceroaugusto@gmail.com>
*/
class AvatarWATC : public InGE::Avatar {
	private:

		enum m_keys {
			WATC_FORWARD=1,
			WATC_BACKWARD=2,
			WATC_STRAFE_LEFT=4,
			WATC_STRAFE_RIGHT=8,
			WATC_TURN_LEFT=16,
			WATC_TURN_RIGHT=32
		};
		int m_keysPressed;
		int m_team;
		
		vector<AudioSource *> m_sources;
		
		static const int M_BULLET_TIMEOUT = 2000;
		
		static const int M_BULLET_DAMAGE = 1;

        string last_idle;
        string idle();
		
		bool m_termThread;
		
		bool m_dead;
		float m_deadTime;
		bool m_respawn;
		bool m_walking;
		int m_life;
		
		queue <unsigned int> m_qBulletCreationInstant;
		queue <Object3D *> m_qBulletInstance;
		
		SDL_Thread* m_pThreadBulletRemover;
		
		static int bulletRemover(void* instance);
		
		int m_bulletSpeed; 
		void fire();
		
		bool weaponCanFire(float currentTime);
		
		GlutLabel *m_label;

		/**
		 * Ã?ltima vez que foi feito um disparo pelo avatar
		 */
		float m_lastFireTime;
		float m_lastWalkSoundTime;
		

		void performWalk();
		void performJump();
		void controlEventHelper(bool &moviment, bool &movimentOpposed, int &keysPressed, int eventCode, int eventCodeOpposed, bool paramState);
		
		void takeDamage(int damageAmount);

		void sendSound(int number);
		void fireSound();
		
		void respaw();
	public:
		AvatarWATC(PhysicalGeom *geom = NULL, int life = 8);
		
		~AvatarWATC();
		
		int getLife();
		int getTeam();
		bool getRespawn();
		
		void setRespawn(bool respawn);
		
		void setModel(IModel *pModel);
		
		void	controlEventHandler(ControlEnum e, ControlParam *param);

		void draw();
		void playSound(int number);

		virtual void onObjectCollision(Object3D* object3d = NULL, PhysicalContactPoint *contacts = NULL , unsigned int numOfContactPoints=0);
		virtual void worldTargetWithCollision(PhysicalContactPoint *moveData = NULL);
		virtual void worldDynamicWithCollision(PhysicalContactPoint *moveData = NULL);
		virtual void worldTargetWithoutCollision(PhysicalContactPoint *moveData = NULL);
		virtual void worldDynamicWithoutCollision(PhysicalContactPoint *moveData = NULL);
		
		void update();
		
		void updateLife();
		void updateScore();

		void checkStuff();
		
		void checkDeadBullets();
		
		void checkOwnLife();
		
		void warnOwnDeath();
		
		void setTeam(int team);
};
#endif
