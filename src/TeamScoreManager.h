//
// C++ Interface: TeamScoreManager
//
// Description:
//
//
// Author:  <>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TEAMSCOREMANAGER_H
#define TEAMSCOREMANAGER_H

#include "InGE.h"
#include <string>

#define SERVER 1
#define CLIENT 2


using namespace InGE;
/**
	@author 
*/
class TeamScoreManager
{
private:
	int m_managerType;
	int m_champignonsScore;
	int m_olivesScore;
	
	void handleDeath(NetCustomMessage* deathMsg);
	void playSound(NetCustomMessage* soundMsg);
	void updateScores(NetCustomMessage* updateMsg);
	void broadCastScore();
	
public:
	TeamScoreManager(int type); //TYPE 1 = SERVER; TYPE 2 = CLIENT
  ~TeamScoreManager();
  
  void clearScores();
  void requireScore();
  int getChampignonsScore();
  int getOlivesScore();
  void check();  
  

};

#endif
